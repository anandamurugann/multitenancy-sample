package com.sample.mt.repository;

import org.springframework.data.repository.CrudRepository;

import com.sample.mt.domain.Employee;

public interface EmployeeRepository extends CrudRepository<Employee, Long>{

}
